#include "mph_test.h"
#include "lib/compression_function.h"
#include "lib/mph_core.h"
#include "lib/mph_io.h"

int cf_test() {
    cf_ready(3, 20);
    vector<MPH_ATOM> foo;
    for (int i=0; i<CONSTANT_N; i++)
        foo.push_back(i);
    print_vector(foo);
    foo=cf(foo);
    print_vector(foo);
    return 0;
}



int mph_test() {
//    cf_test();
    mph_haifa("mph_test_input", 3);
    return 0;
}
