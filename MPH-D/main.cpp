/*
    MPH-160
    unit:bit
    s = 8
    n = 40
    m = 20
    h = 160
 */

#include <iostream>
#include <sstream>
#include <cstring>
#include <time.h> //time_t time()  clock_t clock()
#include <fstream>

#include "lib/mph_core.h"
#include "lib/mph_io.h"
#include "lib/mph_difference.h"
#include "lib/compression_function.h"

using namespace std;

int main(int arg, char **args)
{
//    cout << arg << endl;
//    cout << args[1] <<endl;
//    cout << args[2] <<endl;


    bool tips = false;
    if (arg>1) {
        if (strcmp(args[1],"haifa")==0){
            cout << "haifa start" <<endl;
            cout << args[0] << "\t" << args[1] << "\t" << args[2] << "\t" << args[3] << endl;
            clock_t  clockBegin, clockEnd;
            clockBegin = clock();
            /************* MPH WORK **********/
    //                string test_path="test_data/mph_100";
            stringstream str;
            str<<args[2];
            int ranks;
            str>>ranks;
            vector<MPH_ATOM> hash_text = mph_haifa(args[3], ranks);
            clockEnd = clock();
            mph_write(string(args[3])+"_output",hash_text);
            cout << "HAIFA Finish, using "<< clockEnd - clockBegin << "" << endl;
        } else if (strcmp(args[1],"sponge")==0){
            cout << "sponge start" <<endl;
            cout << args[0] << "\t" << args[1] << "\t" << args[2] << "\t" << args[3] << endl;
            clock_t  clockBegin, clockEnd;
            clockBegin = clock();
            /************* MPH WORK **********/
    //                string test_path="test_data/mph_100";
            stringstream str;
            int ranks;
            int out_count;
            str<<args[2];
            str>>ranks;
            str.clear();
            str<<args[5];
            str>>out_count;

            vector<MPH_ATOM> hash_text = mph_sponge(args[3], ranks);
            mph_sponge_extrusion_write(string(args[4]), out_count);
            clockEnd = clock();
            cout << "sponge Finish, using "<< clockEnd - clockBegin << "" << endl;

        } else if (strcmp(args[1],"difference")==0){
            cout << "difference" << endl;
            /*************difference start**********/
            mph_cf_difference_hack(3);
        } else if (strcmp(args[1],"file_difference")==0){
            cout << "file_difference tbc" << endl;
        } else if (strcmp(args[1],"export_sponge")==0) {
            stringstream str;
            int ranks;
            str<<args[2];
            str>>ranks;
            int sponge_m = CONSTANT_N;

            cf_ready(ranks, sponge_m);

            string paras = export_paras();

            ofstream outfile;
            outfile.open(args[3],ofstream::out | ofstream::binary);
            if (outfile.is_open()){
                outfile.write(paras.c_str(),paras.size());
            }
            outfile.close();
        } else {
            tips = true;
        }
    }  else tips = true;

    if (tips) {
        cout << "USAGE: [command]" << endl;
        cout << "    command:"<<endl;
        cout << "        haifa mph_rank file_path    使用mph_rank阶的haifa模式计算file_path" << endl;
        cout << "        sponge mph_rank file_path out_path out_byte    使用mph_rank阶的sponge模式计算file_path,输出out_byte到out_path" << endl;
        cout << "        difference mph_rank   进行mph_rank阶的压缩函数差分区分攻击" << endl;
        cout << "        file_difference mph_rank   进行mph_rank阶的哈希函数差分区分攻击" << endl;
        cout << "        export_sponge mph_rank out_path    导出mph_rank阶的sponge模式的参数" << endl;
    }
    return 0;
}


/**
 * bin\debug\MPH-D.exe sponge 3 test_data\mph_1  result  100
 * bin\debug\MPH-D.exe haifa 3 test_data\mph_1
 * bin\debug\MPH-D.exe export_sponge 8 result
 * bin\debug\MPH-D.exe difference 3
 *
 */

