#ifndef COMMON_VAR_H_INCLUDED
#define COMMON_VAR_H_INCLUDED

#define MPH_UNIT unsigned short   //16bit
								//被作为以下单位使用: 项数下标
#define MPH_ATOM unsigned char		//8bit
								//被作为以下单位使用: 系数，gf28运算数
#define MPH_COUNT int

//define debug flag
//#define MPH_DEBUG

#include<vector>
#include<string>
#include<assert.h>
#include<stdlib.h>

#include<iostream>
using namespace std;

const MPH_COUNT UnitLimit = 0x10000;

//const MPH_COUNT CONSTANT_S = 8;    //单元比特数

const MPH_COUNT CONSTANT_N = 40;    //CF函数输入byte数
//const MPH_COUNT CONSTANT_M = 20;    //CF函数输出byte数/方程数

//const MPH_COUNT CONSTANT_H = 160;   //
const MPH_COUNT CONSTANT_Q = 256; //有限域范围
//const MPH_COUNT CONSTANT_D = 3;     //差分阶数


/********************* Tools **********************/

vector<MPH_ATOM> vector_concatenate(vector<MPH_ATOM> a, vector<MPH_ATOM> b);

int vector_xor(vector<MPH_ATOM>& a, vector<MPH_ATOM>& b);
vector<MPH_ATOM> vector_add(vector<MPH_ATOM> a, vector<MPH_ATOM> b);
vector<MPH_ATOM> vector_red(vector<MPH_ATOM> a, vector<MPH_ATOM> b);

int print_vector(vector<MPH_ATOM> foo);

#endif // COMMON_VAR_H_INCLUDED
