#ifndef MPH_IO_H_INCLUDED
#define MPH_IO_H_INCLUDED

#include "common_var.h"

/**
 * 设置读取路径
 * @param  t_path 读取文件路径
 * @return        null
 */
int read_set_path(string t_path);

/**
 * size:读取的Byte数
 * read_status 实际读取的Byte数
 */
vector<MPH_ATOM> read_next(int size, int& read_status);

/**
 * 将hashed_value写入到out_path
 * @param  out_path     输出路径
 * @param  hashed_value hash结果值
 * @return              null
 */
int mph_write(string out_path,vector<MPH_ATOM> hashed_value, bool append=false);

/**
 * 返回是否读取完毕
 * @return 1:读取完毕;0:还没读取完
 */
int end_of_read();

/**
 * 读取完毕处理，关闭文件流
 * @return null
 */
int read_done();

#endif // MPH_IO_H_INCLUDED
