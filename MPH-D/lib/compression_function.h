#ifndef COMPRESSION_FUNCTION_H_INCLUDED
#define COMPRESSION_FUNCTION_H_INCLUDED

#include "common_var.h"

// 输入为Mi，长度为n*s,
vector<MPH_ATOM> cf(vector<MPH_ATOM> foo);

string export_paras();

int cf_ready(int mph_rank, int formula_num);

const MPH_COUNT Terms4=200;

const MPH_COUNT Terms=200;

const MPH_COUNT RANK_LIMIT = 10;

extern const MPH_COUNT High_Terms[RANK_LIMIT];

extern const MPH_COUNT Terms3Limit ;



#endif // COMPRESSION_FUNCTION_H_INCLUDED
