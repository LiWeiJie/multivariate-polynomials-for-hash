#include "common_var.h"

vector<MPH_ATOM> vector_concatenate(vector<MPH_ATOM> a, vector<MPH_ATOM> b) {
    vector<MPH_ATOM> ab;
    ab.reserve(a.size()+b.size());
    ab.insert(ab.end(),a.begin(),a.end());
    ab.insert(ab.end(),b.begin(),b.end());
    return ab;
}

int vector_xor(vector<MPH_ATOM>& a, vector<MPH_ATOM>& b){
    int alen = a.size();
    int blen = b.size();
    if (alen<=blen) {
        for (int i=0; i < alen; i++)
            a[i]^=b[i];
        for (int i = 0; i< (blen-alen); ++i)
            a.push_back(b[alen+i]);
    } else {
        for (int i=0; i < blen; i++)
            a[i]^=b[i];
    }

    return 0;
}

vector<MPH_ATOM> vector_add(vector<MPH_ATOM> a, vector<MPH_ATOM> b){
    vector<MPH_ATOM> c;
    for (unsigned int i=0; i < a.size(); i++)
        c.push_back(a[i]+b[i]);
    return c;
}

vector<MPH_ATOM> vector_red(vector<MPH_ATOM> a, vector<MPH_ATOM> b){
    vector<MPH_ATOM> c;
    for (unsigned int i=0; i < a.size(); i++)
        c.push_back((CONSTANT_Q+a[i]-b[i])%CONSTANT_Q);
    return c;
}

int print_vector(vector<MPH_ATOM> foo) {
    vector<MPH_ATOM>::iterator iter = foo.begin();
    cout << foo.size() << ":" << endl;
    for (; iter!=foo.end(); iter++) {
        cout << " " << (int)*iter;
    }
    cout << endl;
    return 0;
}
