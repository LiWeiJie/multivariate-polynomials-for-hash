#ifndef MPH_CORE_H_INCLUDED
#define MPH_CORE_H_INCLUDED

#include "common_var.h"

vector<MPH_ATOM> mph_haifa(string t_path, int mph_rank);
vector<MPH_ATOM> mph_sponge(string t_path, int mph_rank);
vector<MPH_ATOM> mph_sponge_extrusion_write(string out_path, MPH_COUNT out_count);

extern const MPH_ATOM MPH_HAIFA_IV[CONSTANT_N/2];

extern const MPH_ATOM MPH_SPONGE_IV[CONSTANT_N];


#endif // MPH_CORE_H_INCLUDED
