#include "mph_core.h"
#include "compression_function.h"
#include "mph_io.h"

const MPH_ATOM MPH_HAIFA_IV[CONSTANT_N/2] = {
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
};

const MPH_ATOM MPH_SPONGE_IV[CONSTANT_N] = {
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
};

vector<MPH_ATOM> cv[2];
vector<MPH_ATOM> salt[2];
vector<MPH_ATOM> counter[2];
const int MASK = 1;
int pos = 0;

/******************* Initial ***********************/

int mph_haifa_ready(int mph_rank, int haifa_m, const string &t_path) {
    pos = 0;
    for (int i=0; i<haifa_m; i++) {
        cv[pos].push_back(MPH_HAIFA_IV[i]);
    }
    salt[pos].resize(haifa_m,0x00);
    counter[pos].resize(haifa_m,0x00);
    cv[pos^MASK].resize(haifa_m,0x00);
    salt[pos^MASK].resize(haifa_m,0x00);
    counter[pos^MASK].resize(haifa_m,0x00);

    cf_ready(mph_rank, haifa_m);
    read_set_path(t_path);

    return 0;
}


/********************* Main **********************/
vector<MPH_ATOM> mph_haifa(string t_path, int mph_rank) {

    int haifa_m = CONSTANT_N/2;

    mph_haifa_ready(mph_rank, haifa_m, t_path);

    vector<MPH_ATOM> M;
    int ct = 0;
    int actual_read;
    while(!end_of_read()) {
        M = read_next(haifa_m, actual_read);
        if (actual_read==0) break;
        pos ^= MASK;
        ++ct;
        vector_xor(salt[pos^MASK],cv[pos^MASK]);
        vector_xor(M,counter[pos^MASK]);
        cv[pos] = cf(vector_concatenate(salt[pos^MASK],M));
//        cv[pos] = M;
        for (int j=0; j<haifa_m; j++) {
            if (j<haifa_m/2)
                salt[pos][j] = cv[pos^MASK][j]^cv[pos][haifa_m-j-1];
            else
                salt[pos][j] = cv[pos^MASK][j]^cv[pos][j-haifa_m/2];
            counter[pos][haifa_m-j-1] = (ct>>(4*j))&0x0f;
        }
    }
//    cout << ct << "*20B" << endl;  // byte count
    read_done();
    return cv[pos];
}

void mph_sponge_ready(int mph_rank, int sponge_m, string t_path) {
    pos = 0;
    for (int i=0; i<sponge_m; i++) {
        cv[pos].push_back(MPH_SPONGE_IV[i]);
    }
    cv[pos^MASK].resize(sponge_m,0x00);

    cf_ready(mph_rank, sponge_m);
    read_set_path(t_path);

}


vector<MPH_ATOM> mph_sponge(string t_path, int mph_rank) {

    int sponge_m = CONSTANT_N;

    mph_sponge_ready(mph_rank, sponge_m, t_path);

    vector<MPH_ATOM> M;
    int ct = 0;
    int actual_read;
    bool first = true;

    while(!end_of_read()) {
        M = read_next(sponge_m/2, actual_read);
        if (first) {
            first = false;
            if (actual_read==0) break;
            pos ^= MASK;
            vector_xor(cv[pos^MASK], M);
            cv[pos] = cf(cv[pos^MASK]);
        }
        if (actual_read==0) break;
        pos ^= MASK;
        ++ct;
        vector_xor(cv[pos^MASK], M);
        cv[pos] = cf(cv[pos^MASK]);
    }
    cout << ct << "*20B" << endl;
    read_done();
    return cv[pos];
}

vector<MPH_ATOM> mph_sponge_extrusion_write(string out_path, MPH_COUNT out_count) {
    MPH_COUNT ct = 0;
    vector<MPH_ATOM> result;
    int sponge_m_half = CONSTANT_N/2;
    bool first = true;
    while(ct<out_count){
        result.push_back(cv[pos][(ct++)%(sponge_m_half)]);
        if ((ct%sponge_m_half)==0){
            pos ^= MASK;
            cv[pos] = cf(cv[pos^MASK]);
        }
        if ((ct%100000)==0) {
            if (first) {
                mph_write(out_path, result, false);
                first = false;
            } else
                mph_write(out_path, result, true);
            result.clear();
        }
    }
    if (first) {
        mph_write(out_path, result, false);
        first = false;
    } else
        mph_write(out_path, result, true);
    return result;
}
