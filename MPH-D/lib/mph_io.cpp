#include<fstream>
#include "mph_io.h"

bool path_defined_flag = false;
bool read_done_flag = true;
string path;
char buffer[102400];

ifstream infile;
ofstream outfile;

/*******************     Utils    ***********************/

/**
 * padding读取数组，设置长度固定为size，不足补0
 * @param  padding_size 数组长度，Byte数
 * @return      实际读入的Byte数
 */
int padding(int padding_size) {
    int buffer_size = infile.gcount();
//    std::cout << buffer_size << endl;
    if (buffer_size!=0&&buffer_size<padding_size)
        for (int i=buffer_size; i<padding_size; i++)
            buffer[i] = 0x00;
    return buffer_size;
}

/**
 * 将char数组包装为vector<MPH_ATOM>,规模为size
 */
vector<MPH_ATOM> pack_mph_atom(int pack_size) {
    vector<MPH_ATOM> result;
    for (int i=0; i<pack_size; i++)
        result.push_back(buffer[i]);
    return result;
}

int unpack_mph_atom(vector<MPH_ATOM> foo) {
    for (unsigned int i=0; i<foo.size(); i++){
        buffer[i] = foo[i];
//        cout << hex << (int)buffer[i];
//        if (i%2==1) cout <<" ";
    }
//    cout << endl;

    return 0;
}

/******************* Status Query ***********************/

int end_of_read() {
    return read_done_flag?1:0;
}

/****************** Status Change ***********************/

int update_read_status() {
    if (path_defined_flag&&infile.eof()==false)
        read_done_flag=false;
    else
        read_done_flag=true;
    return 0;
}

int read_done() {
    if (infile.is_open()==true) {
        read_done_flag = true;
        path_defined_flag = false;
        infile.close();
    }
    return 0;
}

int read_set_path(string t_path) {
    path = t_path;
    infile.open(path.c_str(),ifstream::in|ifstream::binary);
    if (infile.is_open()) {
        path_defined_flag = true;
        read_done_flag = false;
        return 0;
    }
    exit(-1);
}

/******************    Main    ***********************/

vector<MPH_ATOM> read_next(int read_size,int& read_status) {
    assert(path_defined_flag);
    infile.read(buffer,read_size);
    read_status = padding(read_size);
    update_read_status();
    return pack_mph_atom(read_size);
}

int mph_write(string out_path, vector<MPH_ATOM> hashed_value, bool append) {
    unpack_mph_atom(hashed_value);
    if (append)
        outfile.open(out_path.c_str(),ofstream::out | ofstream::binary | ofstream::app);
    else
        outfile.open(out_path.c_str(),ofstream::out | ofstream::binary);
//    cout << hashed_value.size() << endl;
    if (outfile.is_open()){
        outfile.write(buffer,hashed_value.size());
    }

    outfile.close();

    #ifdef MPH_DEBUG
    out_path = out_path+"_visble";
    outfile.open(out_path.c_str(),ofstream::out);
    if (outfile.is_open())
        for (unsigned int i=0; i<hashed_value.size(); i++){
            outfile<<" "<< ((int)hashed_value[i]);
        }
        outfile<<endl;
    outfile.close();
    #endif // MPH_DEBUG
    return 0;
}
