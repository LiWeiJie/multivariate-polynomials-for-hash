#include "mph_difference.h"
#include "compression_function.h"

vector<MPH_ATOM> sample[8];
vector<MPH_ATOM> sample_out[8];
vector<MPH_ATOM> difference[3];
vector<MPH_ATOM> difference_input;
vector<MPH_ATOM> difference_output;
vector<MPH_ATOM> disturb;
MPH_ATOM text[5][41] = {
    "hello,mph~hello,mph~hello,mph~hello,mph~",
    "Nothing is impossible to  willing heart.",
    "For man is man and master of his fate.  ",
    "It is hard to memory a suitable sentense",
    "God, Why I need a more sentense to diff~"
};

int mph_cf_add_hack(int mph_rank);
int mph_cf_xor_hack(int mph_rank);

//利用差分值和基值产生输入值
int gen_add_sample();
//利用差分值和基值产生输入值
int gen_xor_sample();

int init() {
    //差分值
    difference[0].clear();
    difference[0].insert(difference[0].end(), text[0],text[0]+CONSTANT_N);
    difference[1].clear();
    difference[1].insert(difference[1].end(), text[1],text[1]+CONSTANT_N);
    difference[2].clear();
    difference[2].insert(difference[2].end(), text[2],text[2]+CONSTANT_N);
    //输入基值
    sample[0].clear();
    sample[0].insert(sample[0].end(),text[3],text[3]+CONSTANT_N);
    //干扰值
    disturb.clear();
    disturb.insert(disturb.end(),text[4],text[4]+CONSTANT_N);
    difference_input.clear();
    difference_input.resize(CONSTANT_N,0);
    difference_output.clear();
    difference_output.resize(CONSTANT_N/2,0);
    return 0;
}

//计算加法输出差分
//TODO 改进硬编码
vector<MPH_ATOM> cal_add_difference(vector<MPH_ATOM> foo[]) {
    vector<MPH_ATOM> result;
    result = foo[1];
    result = vector_add(result,foo[2]);
    result = vector_add(result,foo[4]);
    result = vector_add(result,foo[7]);
    result = vector_red(result,foo[0]);
    result = vector_red(result,foo[3]);
    result = vector_red(result,foo[5]);
    result = vector_red(result,foo[6]);
    return result;
}



// 程序入口
int mph_cf_difference_hack(int mph_rank) {
    cout << "Add Difference" << endl;
    mph_cf_add_hack(mph_rank);
    cout << endl;
    cout << "Xor Difference" << endl;
    mph_cf_xor_hack(mph_rank);
    cout << endl;
    return 0;
}

int array_xor(vector<MPH_ATOM> foo[], vector<MPH_ATOM> xor_foo, int t_size) {
    for (int i=0; i<t_size ;i++)
        vector_xor(foo[i],xor_foo);
    return 0;
}

int array_add(vector<MPH_ATOM> foo[], vector<MPH_ATOM> add_foo, int t_size) {
    for (int i=0; i<t_size ;i++)
        foo[i] = vector_add(foo[i],add_foo);
    return 0;
}

int mph_cf_xor_hack(int mph_rank) {
    init();
    cf_ready(mph_rank, 20);
    gen_xor_sample();

    vector_xor(difference_input,difference_input);
    vector_xor(difference_output,difference_output);

    for (int i=0;i<8;i++) {
        sample_out[i] = cf( sample[i] );
        vector_xor(difference_output,sample_out[i]);
    }
//    cout << "difference_input:" << endl;
//    print_vector(difference_input);
    cout << "difference_output:" << endl;
    print_vector(difference_output);

    cout << "-----------------------------" << endl;

    vector_xor(difference_output,difference_output);
    //干扰值介入
    array_xor(sample,disturb,8);

    for (int i=0;i<8;i++) {
        sample_out[i] = cf( sample[i] );
        vector_xor(difference_output,sample_out[i]);
    }
//    vector_xor(difference_output,difference_output);
//    cout << "difference_input:" << endl;
//    print_vector(difference_input);
    cout << "difference_output:" << endl;
    print_vector(difference_output);
    return 0;
}

int mph_cf_add_hack(int mph_rank) {

    init();
    gen_add_sample();

    cf_ready(mph_rank, 20);

    for (int i=0;i<8;i++) {
        sample_out[i] = cf( sample[i] );
    }

    difference_output = cal_add_difference(sample_out);

//    cout << "difference_input:" << endl;
//    print_vector(difference_input);
    cout << "difference_output:" << endl;
    print_vector(difference_output);

    cout << "-----------------------------" << endl;

    array_add(sample,disturb,8);
    for (int i=0;i<8;i++) {
        sample_out[i] = cf( sample[i] );
//        print_vector(sample_out[i]);
    }

    difference_output = cal_add_difference(sample_out);
//    cout << "difference_input:" << endl;
//    print_vector(difference_input);
    cout << "difference_output:" << endl;
    print_vector(difference_output);
    return 0;
}

int gen_add_sample() {
    int ct = 0;
    for (int i=0; i<2; i++) {
        vector<MPH_ATOM> temp(sample[0]);
        vector<MPH_ATOM> tmp_i;
        if (i)
            tmp_i = vector_add(temp,difference[0]);
        else
            tmp_i = temp;
        for (int j=0; j<2; j++) {
            vector<MPH_ATOM> tmp_j;
            if (j)
                tmp_j = vector_add(tmp_i,difference[1]);
            else
                tmp_j = tmp_i;
            for (int k=0; k<2; k++) {
                vector<MPH_ATOM> tmp_k;
                if (k)
                    tmp_k = vector_add(tmp_j,difference[2]);
                else
                    tmp_k = tmp_j;
                sample[ct++]=tmp_k;
            }
        }
    }
    return 0;
}

int gen_xor_sample() {
    int ct = 0;
    for (int i=0; i<2; i++) {
        vector<MPH_ATOM> temp(sample[0]);
        if (i)
            vector_xor(temp,difference[0]);
        for (int j=0; j<2; j++) {
            if (j)
                vector_xor(temp,difference[1]);
            for (int k=0; k<2; k++) {
                if (k)
                    vector_xor(temp,difference[2]);
                sample[ct++]=temp;
                if (k)
                    vector_xor(temp,difference[2]);
            }
            if (j)
                vector_xor(temp,difference[1]);
        }
        if (i)
            vector_xor(temp,difference[0]);
    }
    return 0;
}
