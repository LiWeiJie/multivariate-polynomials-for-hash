文件结构说明

./ 
	main	主程序入口

	mph_test	为了方便测试而不影响主体程序而构建，可忽略

	time_test_record	记录时间测试的数据

	test_data/	测试数据
		mph_1	1KB大小的测试文件
		mph_10	10KB大小的测试文件
		...	

	lib/
		common_var	放置一些全局设定和通用变量

		compression_function	压缩函数
			cf_ready()	cf的初始化函数，必须先调用，一次调用，可供cf()多次使用
			cf()	压缩函数，传入原文，返回压缩文	

		mph_core	实现mph主体结构
			mph_go(string path)	调用mph的接口，传入文件地址path，返回最后的hash值

		mph_io	实现对文件的输入输出
			mph_set_path(string path) 设置文件读取入口
			read_next(int& read_status)  read_status返回实际读取字节数，函数返回读取的数据
			end_of_read() 查询文件读取是否完毕
			read_done() 读完关闭文件
			mph_write(string out_path,vector<MPH_ATOM> hashed_value)  向out_path写入hash结果

		mph_difference	差分性质进行测试
			mph_cf_difference_hack() 压缩函数差分测试
