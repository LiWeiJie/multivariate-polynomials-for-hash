
typedef unsigned long MPH_U32;
typedef unsigned char MPH_U8;

__constant int Rank_Count[9] = {1, 40, 820, 200, 400, 400, 400, 200, 300};

__kernel void MPH_Compression(
	__global uchar* plaintext, __global uchar* ciphertext,
	int nrounds, int ranks,
	__global MPH_U8 (*MPH_Modules)[40][9][820],
	__global MPH_U8 (*MPH_Terms)[40][9][820][8],
	__global MPH_U8 (*gf28_mul)[256][256])
{
	int idx = get_global_id(0);
	int idy = get_global_id(1);

	int pt_offset = nrounds*20;
	int ct_offset_xor = (nrounds%2==0)?40:0;
	int ct_offset_put = (nrounds%2==0)?0:40;

	MPH_U8 pt[40];
	for (int i=0; i<20; i++) {
        pt[i] = plaintext[pt_offset+i];
        if (nrounds!=0) pt[i] ^= ciphertext[ct_offset_xor+i];
	}
	for (int i=20; i<40; i++) {
        pt[i] = ciphertext[ct_offset_xor+i];
	}

    MPH_U8 result = 0;
    result = MPH_Modules[0][idx][0][0];
    int ct = 0;
    for (int i=1; i<=ranks; ++i) {

        for (int j=0; j<Rank_Count[i]; ++j) {

            MPH_U8 tm = 1;
            for (int k=0; k<i; ++k) {
                tm = (*gf28_mul)[tm][ pt[ MPH_Terms[0][idx][i][j][k] ] ];
            }
            result ^= tm;
        }
    }
	/*
	* apply last round and
	* map cipher state to byte array block:
	*/
	//ciphertext[ct_offset_put+idx] = result;

	/*����������*/
	if (idx==10){
        int pos = 0;
        while (ciphertext[pos]!=0){
            pos++;
        }
        ciphertext[pos] = nrounds;
	}



}
