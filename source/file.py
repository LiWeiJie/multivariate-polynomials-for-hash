import os
import sys
import ctypes
import struct

file = open('e://aa.txt','rb')
filename = 'e://aa.txt'
size = 20
length = os.path.getsize(filename)
#进行补位基值

#160bit/8 = 20byte,因为getsize返回的是size，每20byte分一组,算出L
l = length/size


#算出进行比特填充后明文的总块数（从0开始计算）
#最后留64位（8byte)填充长度,故最后一步message小于12byte,则补12-mess,不然补20-mess+12
def piece():
    tem = length%size
    if(tem<12):
        return length/size 
    else:
        return length/size + 1

    
#比特填充函数,返回包含20个元素的数组，且每个元素在0到256之间
#以20byte为一块，故可能填充1-2块
def fill_below_12():
    arr = []
    tem = length%size
    point = length-tem
    file.seek(point)
    if(tem<12):       
        for i in range(tem/4):
            buf=file.read(4)
            point += 4
            file.seek(point)
            strTem = struct.pack("4s",buf)
            int_value = struct.unpack("i",strTem)     #return为元组
            value = int_value[0]
            for j in range(4):
                tem1 = value & 0xff
                arr.append(tem1)
                value = value>>8
        if ((tem%4)==0):
            strTem = struct.pack("1s1s1s1s",chr(128),chr(0),chr(0),chr(0))
            intTem = struct.unpack("i",strTem)
            value = intTem[0]
            for j in range(4):
                tem1 = value & 0xff
                arr.append(tem1)
                value = value>>8
        if ((tem%4)==1):
            strTem = struct.pack("1s1s1s1s",file.read(1),chr(128),chr(0),chr(0))
            intTem = struct.unpack("i",strTem)
            value = intTem[0]
            for j in range(4):
                tem1 = value & 0xff
                arr.append(tem1)
                value = value>>8
                point += 1
        if ((tem%4)==2):
            strTem = struct.pack("2s1s1s",file.read(2),chr(128),chr(0))
            intTem = struct.unpack("i",strTem)
            value = intTem[0]
            for j in range(4):
                tem1 = value & 0xff
                arr.append(tem1)
                value = value>>8
                point += 2
        if ((tem%4)==3):
            strTem = struct.pack("3s1s",file.read(3),chr(128))
            intTem = struct.unpack("i",strTem)
            value = intTem[0]
            for j in range(4):
                tem1 = value & 0xff
                arr.append(tem1)
                value = value>>8
                point += 3
        for i in range(20-(point-length+tem)-8):
            arr.append(0)
        tem1 = length * 8
        temp = []
        while(tem1>0):
            temp.append(int(tem1 & 0xff))
            tem1 = tem1>>8
        for i in range(8-len(temp)):
            arr.append(0)
        for i in range(len(temp)):
            arr.append(temp[len(temp)-i-1])
        return arr
    else:
        return -1


def fill_up_12():
    arr = []
    tem = length%size
    point = length-tem
    file.seek(point)
    if(tem>=12):
        for i in range(tem/4):
            buf=file.read(4)
            point += 4
            file.seek(point)
            strTem = struct.pack("4s",buf)
            int_value = struct.unpack("i",strTem)     #return为元组
            value = int_value[0]
            for j in range(4):
                tem1 = value & 0xff
                arr.append(tem1)
                value = value>>8
        if ((tem%4)==0):
            strTem = struct.pack("1s1s1s1s",chr(128),chr(0),chr(0),chr(0))
            intTem = struct.unpack("i",strTem)
            value = intTem[0]
            for j in range(4):
                tem1 = value & 0xff
                arr.append(tem1)
                value = value>>8
        if ((tem%4)==1):
            strTem = struct.pack("1s1s1s1s",file.read(1),chr(128),chr(0),chr(0))
            intTem = struct.unpack("i",strTem)
            value = intTem[0]
            for j in range(4):
                tem1 = value & 0xff
                arr.append(tem1)
                value = value>>8
                point += 1
        if ((tem%4)==2):
            strTem = struct.pack("2s1s1s",file.read(2),chr(128),chr(0))
            intTem = struct.unpack("i",strTem)
            value = intTem[0]
            for j in range(4):
                tem1 = value & 0xff
                arr.append(tem1)
                value = value>>8
                point += 2
        if ((tem%4)==3):
            strTem = struct.pack("3s1s",file.read(3),chr(128))
            intTem = struct.unpack("i",strTem)
            value = intTem[0]
            for j in range(4):
                tem1 = value & 0xff
                arr.append(tem1)
                value = value>>8
                point += 3
        for i in range(20-(point-length+tem)):
            arr.append(0)
        return arr
    else:
        return -1
        
def fill_last():
    arr = []
    tem1 = length * 8
    temp = []
    while(tem1>0):
        temp.append(int(tem1 & 0xff))
        tem1 = tem1>>8
    for i in range(20-len(temp)):
         arr.append(0)
    for i in range(len(temp)):
         arr.append(temp[len(temp)-i-1])
    return arr
    
    

#读取函数，参数i为读取的明文第几块，返回一个包含20个元素的数组，且每个元素在0到256之间
#此时i不包括补位产生的数组(i<piece()-1 or i<piece()-2)
def read(i):
    point = i*20
    file.seek(point)
    arr = []
    for i in range(5):
        buf1 = file.read(4)
        point += 4
        file.seek(point)
        strTem = struct.pack("4s",buf1)
        int_value = struct.unpack("i",strTem)     #return为元组
        value = int_value[0]
        for j in range(4):
            tem = value & 0xff
            arr.append(tem)
            value = value>>8
    return arr


for i in range(l):
    print read(i)
if(length%size<12):
    print fill_below_12()
else:
    print fill_up_12()
    print fill_last()
file.close()


            
