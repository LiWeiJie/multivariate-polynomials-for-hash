# -*- coding: utf-8 -*-
class MPHash:
	n = 40
	s = 8
	q = 256
	Terms3 = 200
	Terms2 = 820
	Terms1 = 40
	is_hashed = 0
	path = ""
	hash_code = []
	terms3 = [ [ 0x25A3, 0x2981, 0x0555, 0x0A05, 0x0700, 0x2599, 0x1AB0, 0x0E00, 0x201F, 0x1532, 0x2C0A, 0x0FC2, 0x1AB0, 0x0578, 0x1F25, 0x2AE0, 0x1CBB, 0x1E42, 0x0253, 0x1856, 0x1214, 0x2B9E, 0x16CC, 0x1DFC, 0x09F4, 0x0CD0, 0x1258, 0x1A08, 0x2927, 0x0328, 0x238C, 0x2300, 0x1A60, 0x2828, 0x26B8, 0x1D9C, 0x0402, 0x12A0, 0x1234, 0x21EE, 0x094C, 0x0732, 0x2A85, 0x2520, 0x289E, 0x26EA, 0x1018, 0x03B1, 0x2BF6, 0x1266, 0x1F80, 0x12BD, 0x041F, 0x12D0, 0x1F95, 0x0956, 0x2B25, 0x2BF1, 0x1ECA, 0x1004, 0x1D48, 0x1B93, 0x13EF, 0x0190, 0x26E9, 0x1230, 0x2AE0, 0x23C4, 0x276A, 0x1F95, 0x04F6, 0x0B50, 0x0D7C, 0x022F, 0x2C06, 0x1A98, 0x1CB6, 0x1689, 0x1037, 0x0F19, 0x1D56, 0x1D5A, 0x146C, 0x2B2A, 0x03D8, 0x03BD, 0x1A4E, 0x04B4, 0x20B8, 0x27B0, 0x0428, 0x09B8, 0x23CD, 0x05AC, 0x11C3, 0x24A8, 0x0320, 0x2A64, 0x1919, 0x2437, 0x05CC, 0x2495, 0x06F1, 0x1628, 0x05EF, 0x2BC0, 0x0408, 0x2B48, 0x10C3, 0x0771, 0x1428, 0x2858, 0x244A, 0x0715, 0x21CF, 0x0292, 0x24B5, 0x2255, 0x2120, 0x1A7C, 0x0228, 0x25CC, 0x2AFD, 0x2CAE, 0x29F8, 0x0444, 0x10C2, 0x2C92, 0x0520, 0x1432, 0x1432, 0x1029, 0x2BC1, 0x04F7, 0x2CA7, 0x17F0, 0x1320, 0x2494, 0x0C04, 0x106C, 0x1554, 0x0A7B, 0x1BE4, 0x1EDF, 0x2258, 0x171C, 0x19AC, 0x19EA, 0x149A, 0x15A0, 0x260A, 0x1EF1, 0x2149, 0x2774, 0x109D, 0x06CE, 0x0EA0, 0x0ECE, 0x109A, 0x0C9E, 0x09C4, 0x15EE, 0x0D9D, 0x1170, 0x1F70, 0x05C8, 0x16A8, 0x067C, 0x159C, 0x1B7C, 0x074C, 0x014B, 0x24EA, 0x0C0C, 0x155E, 0x1AB8, 0x0508, 0x1318, 0x0547, 0x0852, 0x08F8, 0x223B, 0x242C, 0x1E30, 0x25A9, 0x1B53, 0x25C0, 0x22D6, 0x2229, 0x2C14, 0x17B0, 0x0720, 0x0D0A, 0x1E78, 0x0D20, 0x0660, 0x26D9, 0x2872, 0x0BF6, 0x1EDC]  ]		
	C = []
	RF = [0x1,0x2415, 0x02E1, 0x182B, 0x0017, 0x0A75, 0x0337, 0x2BE7, 0x0057, 0x0481, 0x0295, 0x15DF, 0x000D, 0x0781, 0x038B, 0x03F1, 0x010F, 0x1DB1, 0x0049, 0x01DD]	
	terms2 = [ [ 0xD8,0xF0,0x0D,0xBA,0x1A,0x6F,0x02,0xC8,0x68,0x5F,0xE2,0x53,0x88,0xFF,0x46,0x9E,0x7F,0xCD,0x01,0x41,0x58,0x3D,0x03,0x7D,0x20,0xB1,0x7D,0xCD,0x35,0xA6,0x46,0xBD,0xC1,0x2B,0x7E,0xFF,0x81,0x98,0x71,0xBC,0xCB,0x7C,0xBD,0x28,0xCF,0xD7,0xB8,0x59,0x1D,0x8A,0x0C,0x59,0x67,0x0B,0xA4,0x7A,0x88,0x5D,0xCC,0xE9,0xC6,0x40,0x93,0x86,0x54,0xA5,0x74,0xF3,0x1B,0x64,0x36,0xEB,0xBB,0x31,0x11,0xD6,0xCC,0xCD,0x12,0x9A,0x69,0x79,0xC1,0xB3,0xEF,0xB0,0x00,0x86,0x02,0x15,0x36,0xF2,0xD1,0xAB,0xAD,0x29,0x9B,0x44,0x39,0xBE,0x73,0x3C,0x1B,0xFF,0x40,0x66,0x24,0x84,0x07,0x47,0x1E,0x08,0x6D,0x82,0x67,0x78,0x77,0x94,0xDF,0x4A,0x95,0xC1,0x04,0x10,0xDF,0x4E,0x5C,0x4D,0x9E,0xF1,0x14,0x87,0x0E,0x86,0xE6,0x9D,0x62,0x03,0x40,0x5B,0x09,0xAE,0xA2,0x4C,0x30,0x8E,0x47,0xCF,0x88,0x1F,0x09,0x6D,0x49,0x4A,0x7F,0x6A,0x77,0x93,0x68,0x9A,0x64,0x95,0x4C,0x76,0x76,0xAB,0x60,0x6F,0x2A,0xF1,0xAB,0x83,0x59,0x6B,0x67,0x0D,0xBA,0x83,0xAA,0xE1,0xA9,0xB9,0x45,0x98,0x26,0x96,0xEC,0x29,0x21,0xE1,0x98,0xBA,0xF5,0x16,0x2A,0xB5,0x32,0x14,0x51,0x1D,0x9F,0x16,0xAD,0x23,0x7F,0xE4,0x35,0xF4,0x14,0x4B,0xD6,0xD6,0x62,0xCA,0x5C,0x73,0x35,0x8E,0x96,0x74,0x65,0xA1,0xB5,0x52,0x59,0x5E,0xF6,0x69,0xCA,0x28,0x81,0x4F,0xC9,0x5B,0x90,0x63,0x04,0x83,0xD3,0xA9,0x45,0xCC,0x14,0xD5,0x3B,0xBA,0x17,0xA5,0x7D,0x1A,0xC4,0x74,0xCC,0x39,0x87,0x0D,0x98,0x53,0xF8,0xBD,0xC6,0x4E,0xB7,0xAC,0xB1,0xA8,0x9A,0x6C,0x6F,0xAF,0x0B,0xD3,0x55,0xF0,0xAB,0x06,0xBD,0xF1,0x3C,0xD7,0x06,0x23,0xBD,0x51,0xDD,0x12,0xD6,0x90,0xBB,0xDF,0x77,0xD7,0xAF,0xF0,0xBD,0xDC,0x5E,0xE6,0x15,0x66,0x48,0xCF,0xA3,0x1A,0x4D,0x9D,0xE5,0x92,0x6F,0xD8,0x93,0xB0,0xD9,0x8F,0xCA,0x32,0x21,0x94,0x6A,0x86,0xCA,0xE9,0xBD,0xDF,0x0D,0x75,0x59,0x88,0xD8,0xFB,0x47,0x79,0x18,0x4D,0x64,0x28,0x34,0xBD,0x08,0xA1,0xD9,0xD5,0xE6,0xD9,0xFD,0x48,0x33,0x3F,0x1E,0x27,0x0B,0x95,0xCB,0x58,0x10,0xFE,0x66,0x45,0x54,0x58,0x28,0xD1,0x79,0xCB,0x65,0x90,0x16,0xA2,0x12,0x37,0x5D,0x5A,0x84,0x51,0xFB,0xF9,0x7A,0x34,0x40,0xA2,0x5E,0x34,0x80,0x65,0xC8,0x16,0xB9,0x5B,0x44,0xC8,0x75,0xF3,0x55,0x4F,0xF2,0x87,0xBA,0x00,0x81,0x33,0xEF,0xF3,0x2C,0xE6,0xF7,0xB5,0x9B,0xB9,0x59,0x3D,0x39,0x80,0x69,0x21,0x92,0x9F,0xC8,0x0A,0xFF,0x20,0x2E,0x66,0x86,0x02,0x67,0x5C,0xFC,0xD3,0x9C,0x05,0x6C,0x81,0xD4,0xD9,0xBD,0x6D,0xBA,0x67,0x8E,0xC1,0xA1,0x4A,0x65,0x04,0xC5,0x58,0x09,0xEB,0xD4,0x1C,0x2C,0x72,0xAD,0x87,0x4D,0x2C,0x67,0xE2,0xDD,0xD7,0x95,0x07,0xA1,0x34,0xC8,0xD1,0x57,0x16,0x56,0xD1,0x9C,0xB8,0x5A,0x42,0x0D,0x53,0xFC,0x3F,0xAD,0xF0,0xF3,0x34,0x87,0x7B,0x48,0x93,0x94,0x1A,0x5C,0xC8,0xDE,0xBD,0x2C,0x6E,0xE6,0xF5,0xD4,0xC3,0x43,0x09,0x52,0x59,0x8D,0x47,0x8D,0x0F,0x79,0x98,0x96,0x40,0x45,0x57,0x2D,0x3A,0x57,0x63,0x87,0xF2,0x21,0x73,0x4E,0xF6,0x47,0xAA,0xEA,0xA2,0x04,0x6C,0xFF,0x97,0xCA,0x6F,0x36,0x69,0x28,0x15,0x41,0x98,0xF1,0xF9,0x19,0xC2,0x9B,0xC7,0x8A,0x16,0xE5,0x52,0xF2,0x0C,0xBC,0xE0,0x46,0x53,0x60,0xC8,0x55,0x0B,0xC3,0x40,0x51,0x35,0x32,0x70,0x8B,0x68,0x36,0xCB,0x7E,0xC5,0xBD,0x9E,0x0F,0x2D,0x75,0xE9,0x13,0xB1,0x79,0x6F,0x12,0x49,0x2F,0x09,0x4A,0xC9,0x70,0x3F,0xF7,0x12,0xEA,0x19,0xD2,0x85,0xC3,0x33,0xD8,0xBE,0x7F,0x10,0x4C,0x7E,0x24,0xB0,0xFF,0xE8,0x9C,0x65,0xCF,0xF1,0x5A,0xE2,0x6E,0x12,0x41,0x95,0x65,0x3F,0xC1,0x37,0x58,0x1D,0x43,0xA1,0x8B,0x70,0xC7,0xE1,0xA5,0xD2,0xCB,0x8D,0xB4,0xAB,0x6F,0x57,0x72,0x5D,0xD8,0xE2,0xC7,0xBF,0xD2,0xD4,0x8E,0xCD,0xB9,0x2D,0x9C,0x9E,0x99,0xD7,0xFE,0x96,0x94,0x7D,0x83,0xDC,0x84,0xA1,0x7B,0x0E,0xE2,0xE9,0xBD,0x34,0xE7,0xBA,0xEF,0xF3,0xF0,0x08,0x01,0x04,0x27,0x6D,0xFB,0xE4,0x60,0x78,0xF9,0xCC,0x41,0x47,0x73,0xE3,0xA6,0x58,0xC2,0xB3,0x43,0x9C,0xE0,0xD9,0x8C,0xCD,0x70,0x01,0xD1,0x02,0xFE,0x12,0xAE,0x81,0x87,0xA6,0xA2,0xD4,0x3C,0xBD,0xFE,0x1F,0x83,0xB2,0x01,0xB0,0x3F,0x6E,0x41,0xD8,0x51,0xDB,0x4C,0xF9,0x5F,0x9B,0x90,0xE1,0xD3,0xFB,0x78,0x56,0x27,0x22,0xD8,0xF2,0x61,0x8F,0x8B,0x32,0xD9,0xC2,0x5B,0xC9,0x42,0x38,0x20,0x3A,0xE8,0x9D,0x2C,0xA5,0x37,0x3E,0xE7,0xE1,0x7D,0xBD,0xB7,0xCC,0xF1,0x08,0x23,0xDF,0xF3,0x8B,0x37,0x06,0x95,0xA5,0x2A,0xAE,0x5B,0x59,0x3E,0x1F,0x3F,0x47,0xE8,0x0C,0xF8,0xDA,0x31,0x68,0x7C,0xC2,0x5C,0x7C,0xC4,0xA5,0xC9,0x37,0xCF,0x11,0x19,0xC5,0xE9,0xB6,0x93,0x63,0x2D,0xD2,0xC6,0x71,0x49,0xEC,0x70,0xC8,0x81,0xC5,0x9F,0x4F,0xF7,0x8C,0x16,0xC9,0x29,0x51,0xF2,0xF1] ]
	terms1 = [ [0xBB,0xBC,0x8E,0xC9,0x77,0xFF,0xA3,0x2F,0x92,0x09,0xB4,0xB1,0x30,0xFC,0xA9,0xA2,0x2B,0xE2,0xBA,0x6D,0x10,0x9C,0x8E,0x2A,0x6B,0x1A,0xA4,0x6F,0x45,0xD2,0xF3,0x69,0xEA,0x6A,0xCD,0x2B,0xF0,0xB8,0x09,0x45] ]
	terms0 = 0x74
	Terms = 200

	# n元的三次项最大值
	def N3(self,x):
		return x*(x+1)*(x+2)/6

	def N2(self,x):
		return x*(x+1)/2

	def N1(self,x):
		return x

	def __init__(self):
		# 进行算法2计算，产生terms3,term210,C数组。保存待hash文件的地址
		n = self.n		
		m = n/2
		limit3 = self.N3(n)
		limit2 = self.N2(n)
		limit1 = self.N1(n)
		q = self.q
		RF = self.RF
		for i in xrange(1,m):
			t = []
			for j in xrange(self.Terms3):
				t.append(self.terms3[0][j]*RF[i]%limit3)
			self.terms3.append(t)
			t = []
			for j in xrange(self.Terms2):
				t.append(self.terms2[0][j]*RF[i]%limit2)
			self.terms2.append(t)
			t = []
			for j in xrange(self.Terms1):
				t.append(self.terms1[0][j]*RF[i]%limit1)
			self.terms1.append(t)
		# print self.terms3
		# print self.terms2
		# print self.terms1
		# print self.terms0


	# 对于n元的所有三次项，形如XiXjXk(i<=j<=k)中，i = x 的项的数量,这里考虑的是(0<=x<n)
	def CountFirst(self,x,n):
		return (n-x+1)*(n-x)/2

	def x_to_triples(self,x):
		# 将压缩空间的三次项转换为具体的3个下标 
		sum = 0
		i = 0
		n = self.n;
		CountFirst = self.CountFirst
		while sum+CountFirst(i, n)<=x:
			sum = sum+CountFirst(i, n)
			i = i+1
		x = x-sum
		r = x % (n-i)
		l = (x-r)/(n-i)
		if r<l:
			j = n-l
			k = n-r-1
		else:
			j = l+i
			k = r+i
		return i, j, k

	def cf(self,mess):
		# 对长度为s*n比特的信息块进行hash操作
		n = self.n;		
		m = n/2
		q = self.q
		Terms3 = self.Terms3
		Terms2 = self.Terms2
		Terms1 = self.Terms1
		terms3 = self.terms3
		terms2 = self.terms2
		terms1 = self.terms1
		terms0 = self.terms0		
		
		assert(len(mess)==n)
		hash_value = []
		for i in xrange(m):
			tmp = 0
			for j in xrange(Terms3):
				id = self.x_to_triples(terms3[i][j])
				tmp += mess[id[0]]*mess[id[1]]*mess[id[2]]*terms3[n/2-i-1][j]
				tmp %= q
			for j in xrange(Terms2):
				id = self.x_to_triples(terms2[i][j])
				tmp += mess[id[1]]*mess[id[2]]*terms2[n/2-i-1][j]
				tmp %= q
			for j in xrange(Terms1):				
				id = self.x_to_triples(terms1[i][j])				
				tmp +=  mess[id[2]]*terms1[n/2-i-1][j]
				tmp %= q
			tmp += terms0
			tmp %= q
			hash_value.append(tmp)
		return hash_value

#Sample 
m = []
for i in xrange(40):
	m.append(i)
mph = MPHash()
print mph.cf(m)